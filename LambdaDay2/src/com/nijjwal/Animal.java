package com.nijjwal;

@FunctionalInterface
public interface Animal {
	void eat();
}
